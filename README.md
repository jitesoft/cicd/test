# Components

GitLab CI/CD Components for testing.  

This repository is only for direct tests (most should include coverage and full reports), check other repos in parent group
if you wish to find other type of components.

## How to use?

The components in this project makes use of the GitLab CI/CD Components system.  
To use a component, it's included in your file with the `include` keyword and will automatically run on the cases it 
is intended to run.  
  
Example:

```yaml
stages:
  - test

include:
  component: gitlab.com/jitesoft/cicd/test/phpunit@master
  inputs:
    skip_versions: "7.4,8.0"
```

The above snippet will run the `phpunit` component and test the code in the repository with phpunit in php 8.1 and 8.2.  

For full information about the component, check the templates directory.

All jobs are suffixed with `.jitesoft` to make sure they do not clash with your own jobs.

## License

MIT
